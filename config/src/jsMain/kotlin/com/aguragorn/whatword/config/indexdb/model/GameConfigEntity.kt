package com.aguragorn.whatword.config.indexdb.model

import com.aguragorn.whatword.config.model.GameConfig
import kotlinext.js.jso


external interface GameConfigEntity {
    var language: String
    var wordLength: String
    var maxTurnCount: String
}


fun GameConfig.toGameConfigEntity(): GameConfigEntity = jso {
    language = this@toGameConfigEntity.language
    maxTurnCount = this@toGameConfigEntity.maxTurnCount.toString()
    wordLength = this@toGameConfigEntity.wordLength.toString()
}

fun GameConfigEntity.toGameConfig(): GameConfig = GameConfig(
    language = language,
    wordLength = wordLength.toInt(),
    maxTurnCount = maxTurnCount.toInt(),
)
