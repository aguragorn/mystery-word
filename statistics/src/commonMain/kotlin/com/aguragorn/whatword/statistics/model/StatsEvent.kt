package com.aguragorn.whatword.statistics.model

sealed interface StatsEvent {
    object ShareLastResult : StatsEvent
}