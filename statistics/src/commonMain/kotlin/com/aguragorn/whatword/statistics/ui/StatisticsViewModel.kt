package com.aguragorn.whatword.statistics.ui

import com.aguragorn.whatword.config.model.GameConfig
import com.aguragorn.whatword.datetime.today
import com.aguragorn.whatword.statistics.model.Stats
import com.aguragorn.whatword.statistics.model.StatsEvent
import com.aguragorn.whatword.statistics.usecase.GetGameStats
import com.aguragorn.whatword.toaster.ToasterViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import kotlinx.datetime.LocalDate

class StatisticsViewModel(
    private val getGameStats: GetGameStats,
    private val toasterViewModel: ToasterViewModel,
) : CoroutineScope {
    override val coroutineContext = Dispatchers.Main

    private val stats = MutableStateFlow<Stats?>(null)
    val statsState: StateFlow<Stats?> = stats.asStateFlow()

    val showStats: Flow<Boolean> = stats.map { it != null }
    val allowCopyLastResult: Flow<Boolean> = stats.map { it?.lastGameDate == LocalDate.today() }

    private val _events = MutableSharedFlow<StatsEvent>(extraBufferCapacity = Channel.UNLIMITED)
    val events: SharedFlow<StatsEvent> = _events.asSharedFlow()

    private var lastResultString: String = ""
    val lastResultStringState: String get() = lastResultString

    fun showGamesStats(config: GameConfig) = launch {
        stats.value = getGameStats.invoke(config = config)
    }

    fun hideStats() {
        stats.value = null
    }

    fun setLastResultString(lastResultString: String) {
        this.lastResultString = lastResultString
    }

    fun shareLastResult() {
        _events.tryEmit(StatsEvent.ShareLastResult)
    }
}