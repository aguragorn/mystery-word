package com.aguragorn.whatword.about.ui

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow

class AboutViewModel {
    private val isShown = MutableStateFlow(false)
    val isShownState: Flow<Boolean> = isShown.asStateFlow()

    fun showAbout() {
        isShown.value = true
    }

    fun hideAbout() {
        isShown.value = false
    }
}