package com.aguragorn.whatword.web.about

import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import com.aguragorn.whatword.about.ui.AboutViewModel
import com.aguragorn.whatword.keyboard.model.Letter
import com.aguragorn.whatword.web.app.*
import com.aguragorn.whatword.web.app.utils.matchParent
import com.aguragorn.whatword.web.game.currentGameWidth
import com.aguragorn.whatword.web.game.gameWidth
import com.aguragorn.whatword.web.theme.appTheme
import org.jetbrains.compose.web.css.*
import org.jetbrains.compose.web.dom.B
import org.jetbrains.compose.web.dom.P
import org.jetbrains.compose.web.dom.Text

@Composable
fun About(aboutViewModel: AboutViewModel) {
    val theme by appTheme.collectAsState()
    val showAbout by aboutViewModel.isShownState.collectAsState(false)

    if (!showAbout) return

    HStack(attrs = {
        id("about-component")
        style {
            width(matchParent)
            height(matchParent)
            position(Position.Absolute)
            margin(0.px)
            backgroundColor(theme.backgroundColor)
        }
    }) {
        Spacer()
        AboutSurface(aboutViewModel)
        Spacer()
    }
}

@Composable
private fun AboutSurface(aboutViewModel: AboutViewModel) {
    val gameWidth by currentGameWidth().collectAsState(gameWidth())

    VStack(attrs = {
        id("about-surface")
        style {
            width(gameWidth)
            height(matchParent)
        }
    }) {
        PopupHeader("About", onClose = { aboutViewModel.hideAbout() })

        val instructionStyle: StyleBuilder.() -> Unit = {
            fontSize(1.2.em)
        }

        HStack(attrs = {
            style {
                width(gameWidth)
                display(DisplayStyle.Block)
                textAlign("center")
                padding(16.px)
            }
        }) {
            P(attrs = { style(instructionStyle) }) {
                Text("Unlock clues each turn. Guess the Mystery Word in six tries.")
            }
            P(attrs = { style(instructionStyle) }) {
                Text("Press enter to submit your guess for the turn.")
            }
            P(attrs = { style(instructionStyle) }) {
                Text("After each turn, letter box colors will change to show how your guess matches with the Mystery Word.")
            }

            P { Text("Samples") }

            val cellStyle: StyleBuilder.() -> Unit = { marginRight(4.px) }
            val rowStyle: StyleBuilder.() -> Unit = {
                width(matchParent)
                justifyContent(JustifyContent.Center)
            }

            HStack(attrs = { style(rowStyle) }) {
                Cell(Letter('E', Letter.Status.CORRECT), style = cellStyle)
                Cell(Letter('X', Letter.Status.INCORRECT), style = cellStyle)
                Cell(Letter('A', Letter.Status.INCORRECT), style = cellStyle)
                Cell(Letter('C', Letter.Status.INCORRECT), style = cellStyle)
                Cell(Letter('T', Letter.Status.INCORRECT))
            }
            P {
                B { Text("E") }
                Text(" is in the Mystery Word and in the ")
                B { Text("CORRECT") }
                Text(" place.")
            }

            HStack(attrs = { style(rowStyle) }) {
                Cell(Letter('H', Letter.Status.INCORRECT), style = cellStyle)
                Cell(Letter('O', Letter.Status.INCORRECT), style = cellStyle)
                Cell(Letter('L', Letter.Status.MISPLACED), style = cellStyle)
                Cell(Letter('D', Letter.Status.INCORRECT), style = cellStyle)
                Cell(Letter('S', Letter.Status.INCORRECT))
            }
            P {
                B { Text("L") }
                Text(" is in the Mystery Word but in the ")
                B { Text("WRONG") }
                Text(" place.")
            }

            HStack(attrs = { style(rowStyle) }) {
                Cell(Letter('W', Letter.Status.INCORRECT), style = cellStyle)
                Cell(Letter('R', Letter.Status.INCORRECT), style = cellStyle)
                Cell(Letter('O', Letter.Status.INCORRECT), style = cellStyle)
                Cell(Letter('N', Letter.Status.INCORRECT), style = cellStyle)
                Cell(Letter('G', Letter.Status.INCORRECT))
            }
            P {
                Text("All letters are ")
                B { Text("NOT IN") }
                Text(" the Mystery Word.")
            }
        }
    }
}
