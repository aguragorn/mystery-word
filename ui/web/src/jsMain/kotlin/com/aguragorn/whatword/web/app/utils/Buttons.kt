package com.aguragorn.whatword.web.app.utils

import org.jetbrains.compose.web.css.*

val textButton: StyleBuilder.() -> Unit
    get() = {
        border(style = LineStyle.None)
        backgroundColor(Color.transparent)
    }

val coloredButton: StyleBuilder.() -> Unit
    get() = {
        border(style = LineStyle.None)
        borderRadius(4.px)
    }