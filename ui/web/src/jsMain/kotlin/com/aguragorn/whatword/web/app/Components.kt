package com.aguragorn.whatword.web.app

import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import com.aguragorn.whatword.keyboard.model.Letter
import com.aguragorn.whatword.web.app.utils.*
import com.aguragorn.whatword.web.theme.appTheme
import kotlinx.coroutines.flow.map
import org.jetbrains.compose.web.css.*
import org.jetbrains.compose.web.dom.*
import org.w3c.dom.HTMLDivElement

/**
 * Expands to fill the remaining space in the parent element.
 * Will push other children to the edges.
 */
@Composable
fun Spacer(attrs: AttrBuilderContext<*> = {}) {
    Div(attrs = {
        style {
            flexGrow(1)
            minWidth(0.px)
            minHeight(0.px)
        }
        attrs()
    })
}

/**
 * A card with a default elevation
 */
@Composable
fun Card(attrs: AttrBuilderContext<*> = {}, content: @Composable () -> Unit) {
    Div(attrs = {
        classes("card")
        attrs()
    }
    ) {
        content()
    }
}

@Composable
fun Title(text: String, attrs: AttrBuilderContext<*> = {}) {
    val showSmallTitle by currentScreenSize
        .map { it <= ScreenSize.MEDIUM }
        .collectAsState(true)

    HStack(attrs = {
        style {
            width(matchParent)
            alignItems(AlignItems.Center)
            textAlign("center")
        }
        attrs()
    }) {
        Spacer()

        if (showSmallTitle) {
            H5(attrs = {
                style { fontWeight("bold") }
            }) {
                Text(text)
            }
        } else {
            H4(attrs = {
                style { fontWeight("bold") }
            }) {
                Text(text)
            }
        }

        Spacer()
    }
}

/**
 * Stacks children vertically like a column. Wraps content by default.
 */
@Composable
fun VStack(attrs: AttrBuilderContext<*> = {}, content: ContentBuilder<HTMLDivElement>) {
    Div(attrs = {
        style {
            display(DisplayStyle.Flex)
            flexDirection(FlexDirection.Column)
            width(wrapContent)
            height(wrapContent)
        }
        attrs()
    }, content = content)
}

/**
 * Stacks children horizontally like a row. Wraps content by default.
 */
@Composable
fun HStack(attrs: AttrBuilderContext<*> = {}, content: ContentBuilder<HTMLDivElement>) {
    Div(attrs = {
        style {
            display(DisplayStyle.Flex)
            flexDirection(FlexDirection.Row)
            width(wrapContent)
            height(wrapContent)
        }
        attrs()
    }, content = content)
}

@Composable
fun PopupHeader(title: String, onClose: () -> Unit) {
    val theme by appTheme.collectAsState()

    HStack(attrs = {
        id("$title-header")
        style {
            backgroundColor(Color.transparent)
            border(style = LineStyle.None)
            width(matchParent)
            height(wrapContent)
            paddingHorizontal(16.px)
            alignContent(AlignContent.Center)
        }
    }) {
        HStack(attrs = {
            style {
                width(matchParent)
                alignItems(AlignItems.Center)
            }
        }) {
            HStack(
                attrs = {
                    style {
                        width(matchParent)
                        alignItems(AlignItems.Center)
                        flexGrow(1)
                    }
                }
            ) {
                Button(attrs = {
                    style {
                        textButton()
                        cursor("pointer")
                    }
                    onClick { onClose() }
                }) {
                    HStack(attrs = {
                        style {
                            color(theme.colors.foreground)
                            alignItems(AlignItems.Center)
                        }
                    }) {
                        Img(src = "icons/ic_stats_close.svg")
                        Text("Close")
                    }
                }
            }

            Title(text = title,
                attrs = {
                    style {
                        width(matchParent)
                        flexGrow(1)
                    }
                })

            Spacer(attrs = { style { width(matchParent) } })
        }
    }
}

@Composable
fun Cell(
    letter: Letter?,
    style: StyleBuilder.() -> Unit = {},
) {
    val theme by appTheme.collectAsState()

    val cellSize = 48.px

    Div(attrs = {
        style {
            width(cellSize)
            height(cellSize)
            letter?.let {
                color(theme.cellForegroundColorFor(it.status))
                backgroundColor(theme.cellColorFor(it.status))
            }
            border { width = 2.px; this.style = LineStyle.Solid; color = Color("#808080"); }
            textAlign("center")
            display(DisplayStyle.Flex)
            alignContent(AlignContent.Center)
            alignItems(AlignItems.Center)
            justifyContent(JustifyContent.Center)
            lineHeight(cellSize)
            fontFamily("sans-serif")
            fontSize(1.3.em)

            // apply style-overrides set by caller
            style()
        }
    }) {
        Text(letter?.char?.uppercase().orEmpty())
    }
}