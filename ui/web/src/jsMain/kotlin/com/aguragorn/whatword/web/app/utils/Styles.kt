package com.aguragorn.whatword.web.app.utils

import org.jetbrains.compose.web.css.CSSNumeric
import org.jetbrains.compose.web.css.StyleBuilder
import org.jetbrains.compose.web.css.paddingLeft
import org.jetbrains.compose.web.css.paddingRight

fun StyleBuilder.paddingHorizontal(value: CSSNumeric) {
    paddingLeft(value)
    paddingRight(value)
}