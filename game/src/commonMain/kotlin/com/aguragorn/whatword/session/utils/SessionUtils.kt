package com.aguragorn.whatword.session.utils

import com.aguragorn.whatword.grid.model.Word
import com.aguragorn.whatword.keyboard.model.Letter
import com.aguragorn.whatword.session.model.GameSession

val GameSession.isStub: Boolean
    get() = gridState.isEmpty()
            || keyboardState.isEmpty()
            || mysteryWord.word.isEmpty()

val Letter.isNotCorrect: Boolean
    get() = status != Letter.Status.UNKNOWN && status != Letter.Status.CORRECT

val Word.isNotCorrect: Boolean
    get() = letters.any { it.isNotCorrect }

fun Word.isCorrect(wordLength: Int): Boolean {
    return letters.size == wordLength && letters.all { it.status == Letter.Status.CORRECT }
}

val GameSession.isWon: Boolean
    get() = gridState.lastOrNull()?.isCorrect(gameConfig.wordLength) == true

val GameSession.isLost: Boolean
    get() {
        println("$gridState")
        return (gridState.size == gameConfig.maxTurnCount
                && gridState.lastOrNull()?.isNotCorrect == true)
    }

val GameSession.isDone: Boolean
    get() = isWon || isLost


fun GameSession.asString(): String {
    val stringBuilder = StringBuilder("Mystery Word #${mysteryWord.puzzleNumber}\n\n")

    for (round in gridState) {
        for (letter in round.letters) {
            stringBuilder
                .append(
                    when (letter.status) {
                        Letter.Status.CORRECT -> "🟦"
                        Letter.Status.MISPLACED -> "🟧"
                        Letter.Status.INCORRECT -> "⬛️"
                        Letter.Status.UNKNOWN -> ""
                    }
                )
                .append(" ")
        }
        stringBuilder.append("\n")
    }
    stringBuilder.append("\nhttps://aguragorn.gitlab.io/mystery-word/")

    return stringBuilder.toString()
}