package com.aguragorn.whatword.session.usecase

import com.aguragorn.whatword.clipboard.ClipboardService
import com.aguragorn.whatword.session.model.GameSession
import com.aguragorn.whatword.session.utils.asString
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

class PutSessionToClipboard : CoroutineScope {
    override val coroutineContext = Dispatchers.Default

    operator fun invoke(session: GameSession): Job = launch(coroutineContext) {
        val stringBuilder = session.asString()

        ClipboardService.putToClipboard(stringBuilder).join()
        // TODO: call share api when available
    }
}
