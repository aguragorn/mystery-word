package com.aguragorn.whatword.keyboard.model

sealed class KeyboardEvent {
    class KeyTapped(val letter: Letter) : KeyboardEvent()
}