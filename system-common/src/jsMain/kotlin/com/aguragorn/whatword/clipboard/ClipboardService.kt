package com.aguragorn.whatword.clipboard

import kotlinx.browser.window
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

actual object ClipboardService : CoroutineScope {

    override val coroutineContext = Dispatchers.Default

    actual fun putToClipboard(text: String): Job = launch(coroutineContext) {
        window.navigator.clipboard.writeText(text)
    }
}