package com.aguragorn.whatword.clipboard

import kotlinx.coroutines.Job

expect object ClipboardService {
    fun putToClipboard(text: String): Job
}